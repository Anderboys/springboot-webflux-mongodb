package com.example.ander.springmongowebflux.controller;

import com.example.ander.springmongowebflux.model.Persona;
import com.example.ander.springmongowebflux.repository.PersonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

@RestController
@RequestMapping("/api")
public class PersonaController {

    @Autowired
    private PersonaRepository personaRepository;

    @PostMapping("/persona")
    public Mono<Persona> addPersona(@RequestBody Persona persona){
        return personaRepository.save(persona);
    }

    @GetMapping("/persona")
    public Mono<Persona> getPersonaById(@RequestParam("id")String id){
        return personaRepository.findById(id)
                .delayElement(Duration.ofSeconds(4));
    }

    @GetMapping("/personas")
    public Flux<Persona> getPersonas(){
        return personaRepository.findAll()
//                .delayElements(Duration.ofSeconds(4));
                .delaySequence(Duration.ofSeconds(1));
    }

}
