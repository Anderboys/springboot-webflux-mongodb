package com.example.ander.springmongowebflux.repository;

import com.example.ander.springmongowebflux.model.Persona;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface PersonaRepository extends ReactiveMongoRepository<Persona,String>{


}
