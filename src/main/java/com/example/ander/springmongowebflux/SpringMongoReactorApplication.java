package com.example.ander.springmongowebflux;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMongoReactorApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringMongoReactorApplication.class, args);
    }

}
